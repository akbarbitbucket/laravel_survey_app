@extends('layouts.app')

@section('content')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('Survey List') }}</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Title</td>
                                    <td>Description</td>
                                    <td>Action</td>
                                </tr>
                            </thead>

                            <tbody>
                                @if(count($survey_list) > 0)
                                    @foreach($survey_list as $survey)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><a href="{{ route('question.index', $survey->id) }}">{{ $survey->name }}</a></td>
                                            <td>{{ $survey->description }}</td>
                                            <td>
                                                <a href="{{ route('survey.edit', $survey->id) }}"><button class="btn btn-primary"> Edit</button></a>

                                                <button class="btn btn-danger" onclick="event.preventDefault();
                                                        document.getElementById('delete-form-{{ $survey->id }}').submit();"> Delete</button>
                                                <form id="delete-form-{{ $survey->id }}" action="{{ route('survey.delete', $survey->id) }}" method="POST" class="d-none">
                                                    @csrf
                                                    @method("DELETE")
                                                </form>

                                                <a href="{{ route('entries.create', $survey->id) }}"><button class="btn btn-dark"> Take Survey</button></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection
