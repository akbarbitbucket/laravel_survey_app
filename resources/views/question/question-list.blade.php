@extends('layouts.app')

@section('content')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('Question List') }}  <span style="float: right;"> <a href="{{ route('question.create', $survey_id) }}" class="btn btn-info">Add New Question</a></span></div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Question</td>
                                    <td>Type</td>
                                    <td>Action</td>
                                </tr>
                            </thead>

                            <tbody>
                                @if(count($question_list) > 0)
                                    @foreach($question_list as $question)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><a href="#">{{ $question->content }}</a></td>
                                            <td>{{ $question->type }}</td>
                                            <td>
                                                <a href="{{ route('question.edit', $question->id) }}"><button class="btn btn-primary"> Edit</button></a>
                                                <button class="btn btn-danger" onclick="event.preventDefault();
                                                        document.getElementById('delete-form-{{ $question->id }}').submit();"> Delete</button>
                                                <form id="delete-form-{{ $question->id }}" action="{{ route('question.delete', $question->id) }}" method="POST" class="d-none">
                                                    @csrf
                                                    @method("DELETE")
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection
