@extends('layouts.app')

@section('content')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ __('Create Question') }}</div>

                <div class="card-body">
                    <form action="{{ route('question.update', $question->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="content" class="form-label">Question</label>
                            <input type="text" class="form-control" name="content" id="content" value="{{ $question->content }}" placeholder="Question">
                            @error('content')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>


                        <div class="mb-3">
                            <label for="type" class="form-label">Type</label>
                            <select name="type" id="type" class="form-control">
                                <option value="">--- Select Type ---</option>
                                @php
                                    $types  = ['text', 'number', 'textarea', 'select', 'checkbox', 'radio'];

                                    foreach ($types as $k=>$v) {
                                        $selected = ($v == $question->type) ? "selected" : "";
                                        echo '<option value="'.$v.'" '.$selected.'>'.ucfirst($v).'</option>';
                                    }
                                @endphp
                            </select>
                            @error('type')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>

                        <div class="option_area" style="display: block">
                            <button type="button" class="btn btn-info addOption" >Add Option</button>
                            @if($question->type == "select" || $question->type == "checkbox" || $question->type == "radio")
                                @foreach($question->options as $key=>$value)
                                    <div id="item{{ $key + 1 }}" class="mb-3">
                                        <label for="option{{ $key + 1 }}" class="form-label">Option {{ $key + 1 }}</label>
                                        <div class="row">
                                            <div class="col-md-11">
                                                <input type="text" name="options[]" id="option{{ $key + 1 }}" class="form-control" value="{{ $value }}">
                                            </div>
                                            <div class="col-md-1">
                                                @if($key != 0)
                                                    <button type="button" id="{{ $key + 1 }}" class="btn btn-danger removeBtn"> <i class="fa fa-minus"></i></button>
                                                @endif
                                            </div>
                                        </div>
                                        @error('options.{{ $key }}')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                @endforeach
                            @else
                                <div class="mb-3">
                                    <label for="option1" class="form-label">Option 1</label>
                                    <input type="text" name="options[]" id="option1" class="form-control">
                                    @error('options.0')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            @endif

                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
@endsection

@section('script_js')

    <script>

        $(function (){

            $(".addOption").on("click", function () {
                var option = $(".option_area .mb-3").length;
                var html    = '<div id="item'+ (option + 1) +'" class="mb-3">\n' +
                    '                                        <label for="option'+ (option + 1) +'" class="form-label">Option '+ (option + 1) +'</label>\n' +
                    '                                        <div class="row">\n' +
                    '                                            <div class="col-md-11">\n' +
                    '                                                <input type="text" name="options[]" id="option'+ (option + 1) +'" class="form-control">\n' +
                    '                                            </div>\n' +
                    '                                            <div class="col-md-1">\n' +
                    '                                                <button type="button" id="'+ (option + 1) +'" class="btn btn-danger removeBtn"> <i class="fa fa-minus"></i></button>\n' +
                    '                                            </div>\n' +
                    '                                        </div>\n' +
                    '                                        @error("options.'+ option +'")\n' +
                    '                                        <small class="text-danger">{{ $message }}</small>\n' +
                    '                                        @enderror\n' +
                    '                                    </div>';

                $(".option_area").append(html);
            });

            $(document).on("click", ".removeBtn", function(){

                var id = $(this).attr('id');
//                alert(id);
                $("#item"+id).remove();

            });
        })
    </script>
@endsection
