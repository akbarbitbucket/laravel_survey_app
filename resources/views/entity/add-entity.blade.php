@extends('layouts.app')

@section('content')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">{{ $survey->name }}</div>

                <div class="card-body">
                    @if(count($survey->questions) > 0)
                    <form action="#" method="post" enctype="multipart/form-data">
                        @csrf

                        @foreach($survey->questions as $question)

                            @php
                                $option_array   = ['select', 'checkbox', 'radio'];
                                $options    = [];
                                if(in_array($question->type, $option_array)){
                                    //$options    = json_decode($question->options);
                                    $options    = $question->options;
                                }
                            @endphp

                            <div class="mb-3">
                                <label for="answer_value{{ $question->id }}" class="form-label">{{ $question->content }}</label>
                                @if($question->type == "select")
                                    <select name="answer_value" id="answer_value{{ $question->id }}" class="form-control" >
                                        <option value=""> -- Select ---</option>
                                        @for($i=0; $i<count($options); $i++)
                                            <option value="{{ $options[$i] }}">{{ $options[$i] }}</option>
                                        @endfor
                                    </select>
                                @elseif($question->type == "checkbox")
                                    @for($i=0; $i<count($options); $i++)
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="{{ $options[$i] }}" id="answer_value{{ $question->id.$i }}">
                                            <label class="form-check-label" for="answer_value{{ $question->id.$i }}">
                                                {{ $options[$i] }}
                                            </label>
                                        </div>
                                    @endfor
                                @elseif($question->type == "radio")
                                    @for($i=0; $i<count($options); $i++)
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="answer_value" id="answer_value{{ $question->id.$i }}">
                                            <label class="form-check-label" for="answer_value{{ $question->id.$i }}">
                                                {{ $options[$i] }}
                                            </label>
                                        </div>
                                    @endfor
                                @elseif($question->type == "textarea")
                                    <textarea name="answer_value" id="answer_value{{ $question->id }}" class="form-control"  rows="3"></textarea>
                                @else
                                    <input type="{{ $question->type }}" class="form-control" name="content" id="content" value="{{ old('content') }}" placeholder="Answer Here">
                                    @error('content')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                @endif
                            </div>

                        @endforeach

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                    @endif
                </div>
            </div>
        </div>
@endsection

@section('script_js')

    <script>

        $(function (){


        })
    </script>
@endsection
