<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function(){
    Route::get('survey', [App\Http\Controllers\SurveyController::class, 'index'])->name('survey.index');
    Route::get('survey/create', [App\Http\Controllers\SurveyController::class, 'create'])->name('survey.add');
    Route::post('survey/store', [App\Http\Controllers\SurveyController::class, 'store'])->name('survey.store');
    Route::get('survey/{survey}/edit', [App\Http\Controllers\SurveyController::class, 'edit'])->name('survey.edit');
    Route::put('survey/update/{survey}', [App\Http\Controllers\SurveyController::class, 'update'])->name('survey.update');
    Route::delete('survey/delete/{survey}', [App\Http\Controllers\SurveyController::class, 'destroy'])->name('survey.delete');

    // Question
    Route::get('question/{survey}', [App\Http\Controllers\QuestionController::class, 'index'])->name('question.index');
    Route::get('question/{survey}/create', [App\Http\Controllers\QuestionController::class, 'create'])->name('question.create');
    Route::post('question/{survey}/store', [App\Http\Controllers\QuestionController::class, 'store'])->name('question.store');
    Route::get('question/{question}/edit', [App\Http\Controllers\QuestionController::class, 'edit'])->name('question.edit');
    Route::put('question/update/{question}', [App\Http\Controllers\QuestionController::class, 'update'])->name('question.update');
    Route::delete('question/delete/{question}', [App\Http\Controllers\QuestionController::class, 'destroy'])->name('question.delete');


    //Entity
    Route::get('entries/{survey}/create', [App\Http\Controllers\EntityController::class, 'create'])->name('entries.create');

});
//Route::get('student', [App\Http\Controllers\StudentController::class, 'index']);
//Route::post('student/store', [App\Http\Controllers\StudentController::class, 'store']);
//Route::get('student/{student}/edit', [App\Http\Controllers\StudentController::class, 'edit']);
//Route::put('student/update/{student}', [App\Http\Controllers\StudentController::class, 'update']);
//Route::delete('student/delete/{student}', [App\Http\Controllers\StudentController::class, 'delete']);
