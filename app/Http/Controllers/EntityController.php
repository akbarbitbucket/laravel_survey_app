<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Illuminate\Http\Request;

class EntityController extends Controller
{
    public function create(Survey $survey)
    {
        $data   = [];
        $data['survey'] = $survey->load(['questions']);

        return view('entity.add-entity', $data);
    }
}
