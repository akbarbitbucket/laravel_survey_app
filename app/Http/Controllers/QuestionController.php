<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Survey;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index(Survey $survey)
    {
        $data   = [];
        $data['survey_id']  = $survey->id;
        $data['question_list']  = $survey->questions;

//        dd($survey->questions);
        return view('question.question-list', $data);

    }
    public function create(Survey $survey)
    {
        $data   = [];
        $data['survey_id']   =  $survey->id;
        return view('question.add-question', $data);
    }


    public function store(Request $request, Survey $survey)
    {
        $option_array   = ['select', 'checkbox', 'radio'];

        if(in_array($request->input('type'), $option_array)){
            $request->validate([
                'content'   => 'required',
                'type'      => 'required',
                'options.*'  => 'required'
            ]);

            $data   =   [
                'content'   => $request->input('content'),
                'type'      => $request->input('type'),
                'options'   => $request->input('options')
            ];

        }else{
            $request->validate([
                'content'   => 'required',
                'type'      => 'required',
            ]);



            $data   =   [
                'content'   => $request->input('content'),
                'type'      => $request->input('type'),
            ];
        }

        $save_question  = $survey->questions()->create($data) ? true : false;

        if ($save_question) {
            $this->setMessage('Question Create Successfully', 'success');
            return redirect()->route('question.index', $survey->id);
        } else {
            $this->setMessage('Question Create Failed', 'danger');
            return redirect()->back()->withInput();
        }

    }

    public function edit(Question $question)
    {
        $data   = [];
        $data['survey_id']  = $question->survey_id;
        $data['question']   = $question;

        return view('question.edit-question', $data);
    }

    public function update(Request $request, Question $question)
    {
        $option_array   = ['select', 'checkbox', 'radio'];

        if(in_array($request->input('type'), $option_array)){
            $request->validate([
                'content'   => 'required',
                'type'      => 'required',
                'options.*'  => 'required'
            ]);

            $data   =   [
                'content'   => $request->input('content'),
                'type'      => $request->input('type'),
                'options'   => $request->input('options')
            ];

        }else{
            $request->validate([
                'content'   => 'required',
                'type'      => 'required',
            ]);



            $data   =   [
                'content'   => $request->input('content'),
                'type'      => $request->input('type'),
            ];
        }

        $update_question  = $question->update($data) ? true : false;

        if ($update_question) {
            $this->setMessage('Question Update Successfully', 'success');
            return redirect()->route('question.index', $question->survey_id);
        } else {
            $this->setMessage('Question Update Failed', 'danger');
            return redirect()->back()->withInput();
        }

    }

    public function destroy(Question $question)
    {
        $delete_question = $question->delete() ? true : false;

        if ($delete_question) {
            $this->setMessage('Question Delete Successfully', 'success');
            return redirect()->route('question.index', $question->survey_id);
        } else {
            $this->setMessage('Question Delete Failed', 'danger');
            return redirect()->back()->withInput();

        }
    }
}
