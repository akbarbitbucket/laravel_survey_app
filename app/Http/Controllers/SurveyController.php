<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{

    public function index()
    {
        $data   = [];
        $data['survey_list']    = Survey::all();

        return view('survey.survey-list', $data);
    }

    public function create()
    {
        $data   = [];

        return view('survey.add-survey');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required'
        ]);

        $data   = [
            'name'          => $request->input('name'),
            'description'   => $request->input('description')
        ];

        $save_survey = Survey::create($data) ? true : false;

        if ($save_survey) {
            $this->setMessage('Survey Create Successfully', 'success');
            return redirect()->route('survey.index');
        } else {
            $this->setMessage('Survey Create Failed', 'danger');
            return redirect()->back()->withInput();
        }
    }

    public function edit(Survey $survey)
    {
        $data   = [];
        $data['survey'] = $survey;

        return view('survey.edit-survey', $data);
    }

    public function update(Request $request, Survey $survey)
    {
        $request->validate([
            'name'  => 'required'
        ]);

        $data   = [
            'name'          => $request->input('name'),
            'description'   => $request->input('description')
        ];

        $update_survey  = $survey->update($data) ? true : false;
        if ($update_survey) {
            $this->setMessage('Survey Update Successfully', 'success');
            return redirect()->route('survey.index');
        } else {
            $this->setMessage('Survey Update Failed', 'danger');
            return redirect()->back()->withInput();
        }
    }


    public function destroy(Survey $survey)
    {
        $delete_survey = $survey->delete() ? true : false;

        if ($delete_survey) {
            $this->setMessage('Survey Delete Successfully', 'success');
            return redirect()->back();
        } else {
            $this->setMessage('Survey Delete Failed', 'danger');
            return redirect()->back()->withInput();
        }
    }



}
