<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;

    protected $guarded  = [];


    public function questions()
    {
        return $this->hasMany(Question::class, 'survey_id', 'id');
    }


    public function entities()
    {
        return $this->hasMany(Entity::class, 'survey_id', 'id');
    }


}
